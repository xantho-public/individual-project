from pprint import pprint
import re
from time import sleep
from typing import Any
import requests
from tqdm import tqdm

from utils import dump_to_json_file, read_csv_file, read_json_list_file, sorted_ips  # type: ignore[import-not-found]


def is_internal_ip(ip_address: str) -> bool:
    return ip_address.startswith("192.168")


def try_extract(regex: str, text: str) -> str | None:
    regex_match = re.search(regex, text)
    if regex_match is None:
        return None

    return regex_match.group(1)


def scrape_whois(ip_address: str) -> dict[str, Any]:
    regexes = [
        r"OrgName:\s*(.*)",
        r"org-name:\s*(.*)",
        r"Organization:\s*(.*)",
        r"descr:\s*(.*)",
    ]

    output: dict[str, Any] = {}

    page_text = requests.get(f"https://www.whois.com/whois/{ip_address}").text

    for regex in regexes:
        org_name = try_extract(regex, page_text)
        if org_name is None:
            continue

        output["Organisation"] = org_name
        break

    sleep(2)  # Too fast and it gives you nothing
    return output


def process_outgoing_ips(ip_freq_dict: dict[str, int]) -> list[dict[str, Any]]:
    # Organisation: {frequency: int, addresses: list[str]}
    org_dict: dict[str, dict[str, Any]] = {}
    failed_ips: list[str] = []

    for ip_address, frequency in tqdm(ip_freq_dict.items()):
        scrape_result = scrape_whois(ip_address)

        if len(scrape_result) == 0:
            failed_ips.append(ip_address)
            continue

        organisation = scrape_result["Organisation"]
        if organisation not in org_dict:
            org_dict[organisation] = {"frequency": 0, "addresses": []}

        org_dict[organisation]["frequency"] += frequency
        org_dict[organisation]["addresses"].append(ip_address)

    output_list = [
        {
            "organisation": organisation,
            "frequency": details["frequency"],
            "addresses": sorted_ips(details["addresses"]),
        }
        for organisation, details in org_dict.items()
    ]

    output_list.sort(key=lambda x: x["frequency"], reverse=True)
    print(f"Failed: {repr(failed_ips)}")

    return output_list


def extract_outgoing_ips(packets: list[dict[str, Any]]) -> dict[str, int]:
    output_dict: dict[str, int] = {}
    for packet in packets:
        source_ip = packet["Source"]
        dest_ip = packet["Destination"]
        if not is_internal_ip(source_ip):
            continue
        if is_internal_ip(dest_ip):
            continue

        if dest_ip not in output_dict:
            output_dict[dest_ip] = 0
        output_dict[dest_ip] += 1

    return output_dict


def process_packets(packets: list[dict[str, Any]]) -> list[dict[str, Any]]:
    ip_freq_dict = extract_outgoing_ips(packets)

    ip_freq_file = "output/task1/ip_freq.json"
    dump_to_json_file(ip_freq_file, ip_freq_dict)

    process_result = process_outgoing_ips(ip_freq_dict)

    return process_result


def format_to_table(results_file: str) -> None:
    results = read_json_list_file(results_file)
    for entry in results:
        print(
            "THE_TAB".join(
                [
                    str(entry["frequency"]),
                    entry["organisation"],
                    ", ".join(entry["addresses"]),
                ]
            )
        )


def main() -> None:
    # packets_file = "task1.csv"
    # packets = read_csv_file(packets_file)

    # result = process_packets(packets)
    # results_file = "output/task1/scrape_results.json"
    # dump_to_json_file(results_file, result)

    results_file = "output/task1/scrape_results.json"
    format_to_table(results_file)


if __name__ == "__main__":
    main()
