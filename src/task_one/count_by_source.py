from dataclasses import dataclass
from typing import Any
from utils import dump_to_json_file, read_csv_file, read_json_list_file, sorted_ips  # type: ignore[import-not-found]


@dataclass
class SourceCount:
    ip_addresses: set[str]
    source_count_dict: dict[str, int]
    whois_results: dict[str, Any]

    def as_dict(self) -> dict[str, Any]:
        return {
            "ip_addresses": sorted_ips(self.ip_addresses),
            "source_count_dict": self.source_count_dict,
            "whois_results": self.whois_results,
        }


def update_source_counts(source_count_dict: dict[str, int], ip_address: str) -> None:
    if ip_address in source_count_dict:
        source_count_dict[ip_address] += 1
    else:
        source_count_dict[ip_address] = 1


def count_by_source(
    pcap_list: list[dict[str, Any]], scrape_results: list[dict[str, Any]]
) -> list[dict[str, Any]]:
    source_count_list = [{**entry, "source_counts": {}} for entry in scrape_results]

    local_prefix = "192.168"
    for packet in pcap_list:
        source = packet["Source"]
        dest = packet["Destination"]
        if not source.startswith(local_prefix) or dest.startswith(local_prefix):
            continue

        for source_count in source_count_list:
            if dest not in source_count["addresses"]:
                continue

            update_source_counts(source_count["source_counts"], source)

    return source_count_list


def print_by_source(source_counts: list[dict[str, Any]]) -> None:
    all_sources_set: set[str] = set()
    for entry in source_counts:
        all_sources_set.update(entry["source_counts"].keys())

    all_sources_list = sorted_ips(all_sources_set)

    print("THE_TAB".join(all_sources_list))
    for entry in source_counts:
        print(
            "THE_TAB".join(
                [
                    entry["organisation"],
                    *(
                        str(entry["source_counts"].get(source, 0))
                        for source in all_sources_list
                    ),
                ]
            )
        )


def main() -> None:
    pcap_file = "task1.csv"
    scrape_results_file = "output/task1/scrape_results.json"

    pcap_list = read_csv_file(pcap_file)
    scrape_results = read_json_list_file(scrape_results_file)

    source_count_list = count_by_source(pcap_list, scrape_results)
    print_by_source(source_count_list)

    dump_to_json_file("output/task1/source_count.json", source_count_list)


if __name__ == "__main__":
    main()
