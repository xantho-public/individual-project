import re
from typing import Any
from utils import read_csv_file, dump_to_json_file, sorted_ips  # type: ignore[import-not-found]

EXPECTED_IPS = {
    "172.16.1.10",
    "172.16.1.11",
    "172.16.1.12",
    "172.16.1.41",
    "172.16.2.11",
    "172.16.2.12",
    "172.16.2.15",
    "172.16.2.16",
    "172.16.2.41",
    "172.16.3.11",
    "172.16.3.12",
    "172.16.3.41",
    "172.16.4.11",
    "172.16.4.12",
    "172.16.4.13",
    "172.16.4.14",
    "172.16.4.41",
    "172.16.5.41",
    "172.16.6.6",
}


def is_ip(address: str) -> bool:
    return re.match(r"\d+\.\d+\.\d+\.\d+", address) is not None


def create_ip_graph(csv_contents: list[dict[str, Any]]) -> dict[str, Any]:
    ip_index_map: dict[str, int] = {}
    adj_list: list[set[int]] = []

    def _get_index(address: str) -> int:
        if address not in ip_index_map:
            new_index = len(ip_index_map)
            ip_index_map[address] = new_index
            adj_list.append(set())

        return ip_index_map[address]

    for packet in csv_contents:
        if not is_ip(packet["Source"]) or not is_ip(packet["Destination"]):
            continue

        source_ip = packet["Source"]
        dest_ip = packet["Destination"]

        source_index = _get_index(source_ip)
        dest_index = _get_index(dest_ip)

        adj_list[source_index].add(dest_index)
        adj_list[dest_index].add(source_index)

    index_ip_map = {index: ip for ip, index in ip_index_map.items()}

    return {
        "index_addresses": index_ip_map,
        "adj_list": {
            source: list(neighbours) for source, neighbours in enumerate(adj_list)
        },
    }


def main() -> None:
    full_csv = "task2.csv"
    csv_contents = read_csv_file(full_csv)

    ip_graph = create_ip_graph(csv_contents)
    ip_graph_file = "output/task2/ip_graph.json"

    dump_to_json_file(ip_graph_file, ip_graph)

    ip_addresses = sorted_ips(ip_graph["index_addresses"].values())
    unknown_ip_addresses = [
        address for address in ip_addresses if address not in EXPECTED_IPS
    ]

    print(f"{ip_addresses=}")
    print(f"{unknown_ip_addresses=}")


if __name__ == "__main__":
    main()
