import csv
import json
import os
from pathlib import Path
import re
from typing import Any, Iterable


def is_ip(address: str) -> bool:
    # Good enough
    return re.match(r"\d+\.\d+\.\d+\.\d+", address) is not None


def sorted_ips(ip_list: Iterable[str]) -> list[str]:
    return sorted(ip_list, key=lambda ip_str: tuple(int(x) for x in ip_str.split(".")))


def read_csv_file(csv_file: str | os.PathLike[str]) -> list[dict[str, Any]]:
    with open(csv_file, "r") as file:
        reader = csv.DictReader(file)
        return [row for row in reader]


def dump_to_json_file(json_file: str | os.PathLike[str], contents: Any) -> None:
    Path(json_file).parent.mkdir(parents=True, exist_ok=True)
    with open(json_file, "w") as file:
        json.dump(contents, file, indent=2, default=str)
