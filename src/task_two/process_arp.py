from pprint import pprint
import re
from typing import Any

from utils import read_csv_file  # type: ignore[import-not-found]


def parse_arp_responses(packets: list[dict[str, Any]]) -> None:
    info_regex = r"(.*) is at (.*)"

    ip_to_mac_dict: dict[str, set[str]] = {}
    mac_to_ip_dict: dict[str, set[str]] = {}

    for packet in packets:
        info = packet["Info"]
        info_match = re.match(info_regex, info)

        if info_match is None:
            continue

        ip_address = info_match.group(1)
        mac_address = info_match.group(2)

        ip_to_mac_dict.setdefault(ip_address, set()).add(mac_address)
        mac_to_ip_dict.setdefault(mac_address, set()).add(ip_address)

    pprint(ip_to_mac_dict)
    pprint(mac_to_ip_dict)


def find_arp_responses_from_attacker(packets: list[dict[str, Any]]) -> None:
    info_regex = r"(.*) is at (.*)"
    attacker_ip = "172.16.20.10"
    attacker_mac = "08:00:27:b3:b4:75"

    for packet in packets:
        info = packet["Info"]
        info_match = re.match(info_regex, info)

        if info_match is None:
            continue

        ip_address = info_match.group(1)
        mac_address = info_match.group(2)

        if mac_address != attacker_mac:
            # Not from the attacker
            continue
        if ip_address == attacker_ip:
            # An honest reply; not an ARP spoof
            continue

        print(packet)


def main() -> None:
    arp_csv = "task2_arp.csv"
    csv_contents = read_csv_file(arp_csv)

    # parse_arp_responses(csv_contents)
    find_arp_responses_from_attacker(csv_contents)


if __name__ == "__main__":
    main()
